%include "dict/dict.inc"
%include "io-lib/lib.inc"
%include "words/words.inc"


%define BUFFER_SIZE 256

section .rodata
    read_error_message: db "READ ERROR: Buffer overflowed when reading stdin for the search key string", 0xA, 0x0
    search_error_message: db "SEARCH ERROR: Entry with the inputted key was not found", 0xA, 0x0

section .bss
    buffer: resb BUFFER_SIZE

section .text

global _start
_start:
    mov rdi, buffer         ;pointer to buffer as first arument
    mov rsi, BUFFER_SIZE    ;length of buffer
    call read_line          ;reading stdin for search key string

    test rax, rax           ;if read_line returned zero then
    jz .read_error          ;jump to read error handling

    mov rdi, link           ;the link to the dictionary
    mov rsi, rax            ;the entry key we are serching for
    call find_word

    test rax, rax           ;check if we found something
    jz .search_error

    add rax, 8              ;get the key pointer
    push rax                ;and save it

    mov rdi, rax            ;get the key length
    call string_length
    pop rdi                 ;compute the description location
    add rdi, rax
    inc rdi


    mov rsi, 1              ;set the stdout
    call print_string       ;print the description
    call print_newline

    xor rdi, rdi            ;return 0
    jmp exit

    .read_error:
        mov rdi, read_error_message     ;print read_error_meassage
        mov rsi, 2                      ;to std_err
        call print_string

        mov rdi, 1                      ;return 1
        jmp exit
    .search_error:
        mov rdi, search_error_message   ;print search_error_message
        mov rsi, 2                      ;to std_err
        call print_string

        mov rdi, 2                      ;return 2
        jmp exit
