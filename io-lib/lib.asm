%define EXIT_SYSCALL 60
%define READ_SYSCALL 0
%define WRITE_SYSCALL 1

%define STDIN 0
%define STDOUT 1

%define SPACE_CHAR 0x20
%define TAB_CHAR 0x9
%define NEWLINE_CHAR 0xA


section .text

global string_length
global read_line
global string_copy
global parse_int
global parse_uint
global read_word
global read_char
global string_equals
global print_int
global print_uint
global print_char
global print_newline
global print_string
global exit
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    cmp rsi, 1          ;checking if rsi is stdout or stderr
    je .skip
    cmp rsi, 2
    je .skip

    mov rax, 0          ;if not exit with 0 in rax
    ret

    .skip:
    push rsi
    push rdi            ;saving the passed string in stack
    call string_length
    pop rdi             ;getting the string back from stack

    mov rsi, rdi        ;string
    mov rdx, rax        ;length
    mov rax, WRITE_SYSCALL
    pop rdi
    syscall

    mov rax, 1
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_CHAR

; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ;saving the char to output in stack

    mov rsi, rsp        ;passing the stack address as the write argument
    mov rdx, 1          ;length
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    syscall

    pop rdi             ;fixing stack
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    sub rsp, 24                 ;allocate stack

    mov rcx, 20                 ;counter
    mov byte[rsp + rcx], 0      ;null terminator
    mov rax, rdi                ;move number to rax
    mov r10, 10                 ;divisor

    .loop:
        xor rdx, rdx            ;clear the RDX that stores division results
        div r10                 ;divide by r11(10) | dl = ax / r11; dh = ax % r11
        dec rcx                 ;decrement the counter
        add dl, '0'             ;offset dl by the ASCII code of '0' to get the
                                ;ASCII code of the current digit
        mov [rsp + rcx], dl     ;move the ASCII digit to stack
        test rax, rax           ;test if end of number
    jnz .loop

    lea rdi, [rsp + rcx]        ;address of the resut string
    call print_string

    add rsp, 24                 ;clear stack
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0                  ;compare to zero and set flags
    jge .end                    ;if positive jump to printing the number with print_uint
                                ;otherwise:
        push rdi                ;save rdi

        mov rdi, '-'            ;print the '-' sign with
        call print_char         ;print_char function

        pop rdi                 ;get the rdi back
        neg rdi                 ;invert its sign and print it with
    .end:                       ;print_uint function
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
   xor rcx, rcx                     ;counter for iterating string
   .loop:
        mov r10b, byte[rdi + rcx]   ;getting the char of the first string

        cmp r10b, byte[rsi + rcx]   ;comparing it to the char of the second string
        jne .not_equal              ;jumping if they are not equal

        cmp r10b, 0                 ;checking for end of string
        je .end                     ;if end of string jump to end

        inc rcx                     ;incresing counter to then get the next char
        jmp .loop

    .not_equal:
        xor rax, rax                ;returning 0 if not equal
        ret

    .end:
        mov rax, 1                  ;returning 1 if equal
        ret
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub rsp, 8          ;allocate stack

    mov rax, READ_SYSCALL
    mov rdi, STDIN
    mov rsi, rsp        ;setting the stack buffer for read syscall
    mov rdx, 1          ;amout of bytes to read
    syscall

    cmp rax, -1         ;check for fail
    je .failed_or_eof

    test rax, rax       ;check for eof
    jz .failed_or_eof

    mov al, [rsp]       ;copy the result from stack buffer
    jmp .end

    .failed_or_eof:
    xor rax, rax        ;return 0 if failed or eof

    .end:
    add rsp, 8          ;restore stack
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14

    mov r12, rdi		        ;buffer pointer
    mov r13, rsi		        ;buffer length
    xor r14, r14                ;length of read word

    cmp r13, 0                  ;if buffer length 0 then
    je .overflow                ;go to overflow handling

    .loop:
        call read_char

        cmp al, SPACE_CHAR
        je .handle_spaces
        cmp al, TAB_CHAR
        je .handle_spaces
        cmp al, NEWLINE_CHAR
        je .handle_spaces

        cmp r14, r13            ;if length of word is bigger or equal to buffer
        jnl .overflow           ;go to overflow handling

        test rax, rax           ;check for eof or failure to read
        jz .end

        mov [r12 + r14], al     ;add new char to buffer
        inc r14                 ;increase word length

    jmp .loop

    .handle_spaces:
        test r14, r14           ;check if we actually read something
    jz .loop

    .end:
        cmp r14, r13            ;check if null-teminator can fit in buffer
        jnl .overflow

        mov byte[r12 + r14], 0  ;add the 0-terminator
        mov rax, r12            ;pointer to string
        mov rdx, r14            ;length of the string

        pop r14                 ;resore calee-saved registers
        pop r13
        pop r12
    ret

    .overflow:
        xor rax, rax
        pop r14
        pop r13
        pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10

    .loop:
        mov r10b, byte[rdi + rdx]   ;get the next char
        sub r10b, '0'               ;offset it by the ASCII code of 0 so we get the actual digit
        cmp r10b, 9                 ;check if it is a digit
        ja .end                     ;if not digit return

        imul rax, rax, 10           ;offset the resulting number by ten to then
        add rax, r10                ;add the next number in a smaller unit place

        inc rdx                     ;increase the counter
    jmp .loop

    .end:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10

    mov r10b, [rdi]                 ;get the first digit

    cmp r10b, '+'
    jne .minus_check                ;if the first char in a plus
    inc rdi                         ;then increase the length
    jmp .positive                   ;and handle as a uint

    .minus_check:
    cmp r10b, '-'                   ;check if its a minus
    jne .positive                   ;if not minus parse as uint with parse_uint
    inc rdi                         ;else parse with uint and invert
    call parse_uint
    inc rdx                         ;incrementing size because of minus
    neg rax                         ;invert
    ret

    .positive:
    call parse_uint
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi

    cmp rax, rdx                        ;if string length is bigger than buffer
    jae .overflow                       ;then goto overflow handling

    xor rax, rax
    xor r10, r10

    .loop:
        mov r10b, [rdi + rax]           ;get the next character of the stirng
        mov byte [rsi + rax], r10b      ;move that character to the buffer

        inc rax                         ;increase counter

        test r10b, r10b                 ;if we hit a null-terminator
    jnz .loop                           ;return

    ret

    .overflow:
        xor rax, rax                    ;return 0 if overflow
        ret
    ret


read_line:
    push r12
    push r13
    push r14

    mov r12, rdi		        ;buffer pointer
    mov r13, rsi		        ;buffer length
    xor r14, r14                ;length of read word

    cmp r13, 0                  ;if buffer length 0 then
    je .overflow                ;go to overflow handling

    .loop:
        call read_char

        cmp al, NEWLINE_CHAR
        je .handle_spaces

        cmp r14, r13            ;if length of word is bigger or equal to buffer
        jnl .overflow           ;go to overflow handling

        test rax, rax           ;check for eof or failure to read
        jz .end

        mov [r12 + r14], al     ;add new char to buffer
        inc r14                 ;increase word length

    jmp .loop

    .handle_spaces:
        test r14, r14           ;check if we actually read something
    jz .loop

    .end:
        cmp r14, r13            ;check if null-teminator can fit in buffer
        jnl .overflow

        mov byte[r12 + r14], 0  ;add the 0-terminator
        mov rax, r12            ;pointer to string
        mov rdx, r14            ;length of the string

        pop r14                 ;resore calee-saved registers
        pop r13
        pop r12
    ret

    .overflow:
        xor rax, rax
        pop r14
        pop r13
        pop r12
    ret
