.PHONY: clean
.PHONY: test
.PHONY: all

ASM=nasm
ASM_FLAGS=-felf64
LD=ld

all: program test clean

clean:
	rm -f dict/*.o io-lib/*.o *.o program

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

program: io-lib/lib.o dict/dict.o main.o
	$(LD) -o $@ $^

test:
	./test.py