%include "io-lib/lib.inc"

section .text

global find_word
find_word:
    push rbp        ;saving sbp
    mov rbp, rsp    ;initializing sbp for this fucntion

    sub rsp, 16     ;local variables for the string and start address

    mov qword[rbp-16], rdi      ;saving the pointer to the start of the dictionary as a local variable
    mov qword[rbp-8], rsi       ;saving the pointer to the search key string as a local variable

    test rsi, rsi               ;if the pointer to te start of the dictionary is zero then
    jz .no_key                  ;jump to end the function


    .loop:
        mov rdi, qword[rbp-16]      ;get the pointer to current entry of the dictionary
        add rdi, 8                  ;and skip the address part of the entry to get the pointer to the key string start
        mov rsi, qword[rbp-8]       ;get the pointer to the search key string

        call string_equals          ;check if the key string are equal

        test rax, rax               ;test the return value
        jnz .found                  ;if zero wasn't returned then we found our entry

        mov rdi, qword[rbp-16]      ;get the current entry pointer
        mov rdi, qword[rdi]         ;get the pointer to the next entry
        test rdi, rdi               ;check if the pointer to the next element is zero
        jz .no_key                  ;if zero skip the loop
        mov qword[rbp-16], rdi      ;save the pointer to the next entry
    jmp .loop
   .no_key:                        ;if zero then we havent found the entry
        add rsp, 16                 ;restore stack
        pop rbp                     ;restore sbp
        xor rax, rax                ;return 0
        ret
    .found:
        mov rax, qword[rbp-16]      ;copy the pointer to the enty to the rax
        add rsp, 16                 ;restore stack
        pop rbp                     ;restore sbp
        ret                         ;return



